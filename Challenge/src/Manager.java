import java.io.IOException;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/recordController")
public class Manager {
	
	
	private Record record;

	@GET
	@Path("/getAllRecords")
	@Produces(MediaType.APPLICATION_JSON)
	public void GetAllRecords() throws IOException {
		Consume c = new Consume();
		int limit = c.getCoordinates().size();
		Maps[] m = new Maps[limit];
		
		// The purpose of this loop is to show the locations of all the coordinates
		for(int i = 0; i < limit; i++) {
			m[i] = new Maps( c.getCoordinates().get(i) );
		}
	}
	
	@POST
	@Path("/createRecord")
	@Produces(MediaType.APPLICATION_JSON)
	public String createRecord() {
		record = new Record();
		return record.getID();
	}
	
	@GET
	@Path("/obtainRecord")
	@Produces(MediaType.APPLICATION_JSON)
	public Record obtainRecord() {
		return record.getRecord();
	}
}
