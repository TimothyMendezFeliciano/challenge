import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class Consume {

	private String url = "http://34.201.19.114:40003/recordController/getAllRecords";
	private String input;
	private String latToFind = "latitude" + '"' + ":";
	private String longToFind = "longitude" +'"' + ":";
	private StringBuffer response = new StringBuffer();	
	private ArrayList<String> latitude = new ArrayList<>();	
	private ArrayList<String> longitude = new ArrayList<>();
	private final int DELIMITER = 18;
	private ArrayList<String> coordinates = new ArrayList<>();
	
	public Consume() throws IOException {
		this.readData();
	}
	
	// Main function used to read data from the url
	public void readData() throws IOException {
		
		//// Use URL class to create URL and establish a connection with the link.
		//// Then establish a GET request to consume information and read the entire page.
		URL obj = new URL(url);
		HttpURLConnection connection = (HttpURLConnection) obj.openConnection();
		connection.setRequestMethod("GET");
		BufferedReader read = new BufferedReader( new InputStreamReader(connection.getInputStream()));
		
		//// Read the page until there are no more lines
		while(( input = read.readLine()) != null ) {
			response.append(input);
		}
		read.close();
		
		//// With the file having been read, the program now look for latitude and longitude data.
		//// Then appends every coordinate to the coordinate ArrayList
		findCoordinates(latToFind, response, latitude);
		findCoordinates(longToFind, response, longitude);
		fillCoordinates();
		
		System.out.println(coordinates);
	}

	private void findCoordinates(String wordToFind , StringBuffer response, ArrayList<String> plane) {
		int counter = 0;
		int end = response.lastIndexOf(wordToFind);
		int wTFindLength = wordToFind.length();
		while(counter < end) {
			int temp = response.indexOf(wordToFind, counter);
			plane.add(response.substring(temp+ wTFindLength, temp+DELIMITER));
			counter = temp+1;
		}
	}

	private void fillCoordinates() {

		int limit = Math.max(latitude.size(), longitude.size());

		for( int i = 0; i < limit; i++) {
			coordinates.add(latitude.get(i) + ',' + longitude.get(i));
		}
	}
	
	//////// STANDARD GETTERS ////////
	public String getUrl() {
		return url;
	}

	public ArrayList<String> getLatitude() {
		return latitude;
	}

	public ArrayList<String> getLongitude() {
		return longitude;
	}

	public ArrayList<String> getCoordinates() {
		return coordinates;
	}

}
