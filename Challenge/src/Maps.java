import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Maps {
	
	//This is the example I used to base the construction of the url
	//https://maps.googleapis.com/maps/api/geocode/json?latlng=40.714224,-73.961452&key=APIKEY
	
	private static final String APIKEY = "AIzaSyAQyyDY8QZQA7m7FI6uV728eiD98ztlEZs"; // MUST replace API key
	private String url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=";
	private String coordinates;
	private String input;
	private StringBuffer response = new StringBuffer();
	
	// Constructor is designed to be used with the coordinates given.
	public Maps(String coordinates) throws IOException {
		this.coordinates = coordinates;
	}
	
	public void constructURL() {
		url = url + coordinates + "&key=" + APIKEY;
	}
	
	// THis will display in console the JSON response google delivers
	public StringBuffer revGeoCode() throws IOException {
		URL obj = new URL(url);
		HttpURLConnection connection = (HttpURLConnection) obj.openConnection();
		connection.setRequestMethod("GET");
		BufferedReader read = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		
		while(( input = read.readLine()) != null) {
			response.append(input);
		}
		read.close();
		
		return response;
	}

}
