import java.util.UUID;

public class Record {
	
	private String id;
	private String categoryA;
	private String categoryB;
	private String comments;
	private String politicA;
	private String politicB;
	private String politicC;
	private boolean anonymous;
	private String name;
	private String email;
	private String phone;
	private String code_state;
	private String code_city;
	private String gender;
	private String ipAddress;
	private String ipInfo;
	private float date;
	private Object image;
	private String staName;
	private String citName;
	private String citId;
	private String latitude;
	private String longitude;
	private int countItems;
	
	
	
	public Record() {
		
	}
	
	public String createRecord() {
		this.id = UUID.randomUUID().toString();
		categoryA = "Z";
		categoryB = "Z9";
		comments = null;
		politicA = null;
		politicB = null;
		politicC = "conservative";
		anonymous = false;
		name = "";
		email = "";
		phone = "";
		code_state = "1";
		code_city = "999";
		gender = "M";
		ipAddress = null;
		ipInfo = null;
		date = 1550722823000f;
		image = null;
		staName = "Archenland";
		citName = "Stormness Head";
		citId = null;
		latitude = "40.714428";
		longitude = "-73.959442";
		countItems = 0;
		
		return id;
	}
	
	public String getID() {
		return this.id;
	}
	
	public Record getRecord() {
		return this;
	}
		
	
}
