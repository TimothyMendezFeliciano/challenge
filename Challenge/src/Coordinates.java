

public class Coordinates<E> {
	private E longitude;
	private E latitude;
	
	public Coordinates(E x, E y) {
		this.longitude = x;
		this.latitude = y;
	}
	
	public Coordinates() {
		
	}

	public E getLongitude() {
		return longitude;
	}

	public E getLatitude() {
		return latitude;
	}

	public void setLongitude(E longitude) {
		this.longitude = longitude;
	}

	public void setLatitude(E latitude) {
		this.latitude = latitude;
	}
	
	
}
