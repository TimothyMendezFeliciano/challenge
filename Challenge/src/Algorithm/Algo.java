package Algorithm;

public class Algo {

	private Character[] accepted = {'g','G', 'o', 'O', '0', '(', ')', '[', ']', '<', '>', 'l', 'L', '|', 'e', 'E', '3'};
	private int limit1 = accepted.length;
	private String word = "G0<>g|3";
	private int limit2 = word.length();


	public boolean foo() {

		int counter = 0;
		for(int i = 0; i < limit1; i++) {
			for(int j = 0; j < limit2; j++) {
				if(word.charAt(j) == ' ') return false;
				if( accepted[i].equals(word.charAt(j)) ) {
					counter++;
					continue;
				}
			}
		}
		return counter >= 6;
	}

}
